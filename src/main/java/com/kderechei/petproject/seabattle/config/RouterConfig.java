package com.kderechei.petproject.seabattle.config;

import com.kderechei.petproject.seabattle.model.PlayGround;
import com.kderechei.petproject.seabattle.model.User;
import com.kderechei.petproject.seabattle.repository.UserRepository;
import com.kderechei.petproject.seabattle.security.jwt.JwtUtil;
import com.kderechei.petproject.seabattle.util.validator.PlayGroundValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RouterConfig {

    private UserRepository userRepository;
    private JwtUtil jwtUtil;

    public RouterConfig(UserRepository userRepository, JwtUtil jwtUtil) {
        this.userRepository = userRepository;
        this.jwtUtil = jwtUtil;
    }

    @Bean
    public RouterFunction<ServerResponse> routes() {
        return route(POST("/login"), this::handleLogin)
                .andRoute(POST("/playground/init").and(accept(APPLICATION_JSON)), this::initPlayGround);
    }

    private Mono<ServerResponse> handleLogin(ServerRequest serverRequest) {
        return serverRequest.bodyToMono(User.class)
                .flatMap(userBody -> userRepository.getByUsername(userBody.getName())
                        .flatMap(user -> {
                            if (userBody.getPassword().equals(user.getPassword())) {
                                String authorizationHeader = CollectionUtils.isNotEmpty(serverRequest.headers().header(AUTHORIZATION))
                                        ? serverRequest.headers().header(AUTHORIZATION).get(0)
                                        : StringUtils.EMPTY;
                                if (StringUtils.isEmpty(authorizationHeader) || !jwtUtil.validateToken(serverRequest.headers().header(AUTHORIZATION).get(0))) {
                                    return ServerResponse.ok()
                                            .header(AUTHORIZATION, jwtUtil.generateToken(user))
                                            .body(fromObject("Welcome!"));
                                } else {
                                    return ServerResponse.ok()
                                            .body(fromObject("You are already logged in"));
                                }
                            }
                            return ServerResponse.status(HttpStatus.UNAUTHORIZED).build();
                        }));
    }

    private Mono<ServerResponse> initPlayGround(ServerRequest request) {
        return request.bodyToMono(PlayGround.class)
                .flatMap(playGround -> Mono.just(PlayGroundValidator.validate(playGround)))
                .flatMap(body -> ServerResponse.ok()
                        .contentType(APPLICATION_JSON)
                        .body(fromObject(body)))
                .subscriberContext(ctx -> ctx.put("playground", request.bodyToMono(PlayGround.class)));
    }
}
