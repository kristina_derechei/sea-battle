package com.kderechei.petproject.seabattle.model;

import java.util.List;

public class PlayGround {

    private List<Ship> intactShips;
    private List<Ship> injuredShips;

    public PlayGround() {
    }

    public PlayGround(List<Ship> intactShips, List<Ship> injuredShips) {
        this.intactShips = intactShips;
        this.injuredShips = injuredShips;
    }

    public List<Ship> getIntactShips() {
        return intactShips;
    }

    public void setIntactShips(List<Ship> intactShips) {
        this.intactShips = intactShips;
    }

    public List<Ship> getInjuredShips() {
        return injuredShips;
    }

    public void setInjuredShips(List<Ship> injuredShips) {
        this.injuredShips = injuredShips;
    }
}
