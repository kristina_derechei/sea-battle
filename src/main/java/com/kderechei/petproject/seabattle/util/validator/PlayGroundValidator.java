package com.kderechei.petproject.seabattle.util.validator;

import com.kderechei.petproject.seabattle.model.PlayGround;
import com.kderechei.petproject.seabattle.model.Ship;

import java.util.List;

public final class PlayGroundValidator {

    /**
     * Valid playground should have 10 intact ships.
     * All intact ships should not cross with each other.
     * Each ship should be placed only in one direction (horizontally or vertically)
     *
     * @param playGround
     * @return true if input playground is valid
     */
    public static boolean validate(PlayGround playGround) {
        List<Ship> intactShips = playGround.getIntactShips();
        if (intactShips.size() != 10) return false;
        return intactShips.stream()
                .allMatch(ship -> ship.getStartX().equalsIgnoreCase(ship.getEndX()) || ship.getStartY() == ship.getEndY());
    }
}
