package com.kderechei.petproject.seabattle.repository.impl;

import com.kderechei.petproject.seabattle.model.User;
import com.kderechei.petproject.seabattle.repository.UserRepository;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class UserRepositoryImpl implements UserRepository {

    @Override
    public Mono<User> getByUsername(String username) {
        return Mono.just(new User("username", "password"));
    }
}
