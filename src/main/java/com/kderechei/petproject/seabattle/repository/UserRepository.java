package com.kderechei.petproject.seabattle.repository;

import com.kderechei.petproject.seabattle.model.User;
import reactor.core.publisher.Mono;

public interface UserRepository {

    Mono<User> getByUsername(String username);
}
