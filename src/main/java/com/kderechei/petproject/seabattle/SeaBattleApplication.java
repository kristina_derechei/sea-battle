package com.kderechei.petproject.seabattle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeaBattleApplication {

	//TODO(KDerechei 17.12-21.12):
	// - add validation (ships should not cross by the sides and corners)
	// - add login page
	// - add unit tests with JUnit5
	public static void main(String[] args) {
		SpringApplication.run(SeaBattleApplication.class, args);
	}
}

