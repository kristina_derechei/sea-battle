package com.kderechei.petproject.seabattle.handler;

import com.kderechei.petproject.seabattle.model.PlayGround;
import com.kderechei.petproject.seabattle.model.User;
import reactor.core.publisher.Mono;

public interface PlayGroundHandler {
    Mono<Boolean> savePlayGround(PlayGround playGround, User user);
}
